module gitlab.com/Northern-Lights/rdns

go 1.13

require (
	github.com/caddyserver/caddy v1.0.4
	github.com/coredns/coredns v1.6.7
	github.com/golang/protobuf v1.3.2
	github.com/miekg/dns v1.1.27
	google.golang.org/grpc v1.26.0
)
