package main

import (
	_ "github.com/coredns/coredns/plugin/forward"
	_ "github.com/coredns/coredns/plugin/log"

	_ "gitlab.com/Northern-Lights/rdns"

	"github.com/coredns/coredns/core/dnsserver"
	"github.com/coredns/coredns/coremain"
)

var directives = []string{
	"rdns",
	"forward",
	"log",
}

func init() {
	dnsserver.Directives = directives
}

func main() {
	coremain.Run()
}
