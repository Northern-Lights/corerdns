# corerdns

## Description

*corerdns* is a DNS server built on CoreDNS with the simple purpose of forwarding DNS requests to another DNS server, caching the answers, and separately responding to pseudo-reverse DNS requests (simple IP-to-domain)

## Usage

Execute the application with a *Corefile* configuration file containing, at a minimum, the configurations for the *rdns* plugin (external; provided by this Go module) and *forward* plugin.